import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ErrorStateMatcher, MatDialog } from '@angular/material';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { DataApiService } from 'src/app/services/data-api.service';
import Swal from 'sweetalert2';
import { EditarCategoriaComponent } from '../editar-categoria/editar-categoria.component';
import { Router } from '@angular/router';
import { CategoriaModel } from 'src/app/models/categoria.model';



@Component({
  selector: 'app-categoria-lista',
  templateUrl: './categoria-lista.component.html',
  styleUrls: ['./categoria-lista.component.css']
})
export class CategoriaListaComponent implements OnInit {
  constructor(private datapi: DataApiService, public modalCategoria: MatDialog) {
    this.getCategoria();
   }

  ngOnInit() {
  }


  abrirModalEditar(idCat : number) {
    const dialogRef = this.modalCategoria.open(EditarCategoriaComponent, {
      data: { id: idCat },
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("Modal cerrado");
    },(err)=>{
      console.log(err);
    });

  }

  categorias: CategoriaModel[] = [];

getCategoria() {
  this.datapi.getCategorias().subscribe(categorias =>
    {
      this.categorias = categorias;
      console.log(this.categorias);
  });
}
}
