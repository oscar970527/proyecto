import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import Swal from 'sweetalert2';
import { DataApiService } from 'src/app/services/data-api.service';
import { Router } from '@angular/router';
import { CategoriaModel } from 'src/app/models/categoria.model';

interface Alert {
  type: string;
  message: string;
}
const ALERTS: Alert[] = [{
  type: 'success',
  message: 'This is an success alert',
}, {
  type: 'info',
  message: 'This is an info alert',
}, {
  type: 'warning',
  message: 'This is a warning alert',
}, {
  type: 'danger',
  message: 'This is a danger alert',
}, {
  type: 'primary',
  message: 'This is a primary alert',
}, {
  type: 'secondary',
  message: 'This is a secondary alert',
}, {
  type: 'light',
  message: 'This is a light alert',
}, {
  type: 'dark',
  message: 'This is a dark alert',
}
];

@Component({
  selector: 'app-registrar-categoria',
  templateUrl: './registrar-categoria.component.html',
  styleUrls: ['./registrar-categoria.component.css']
})


export class RegistrarCategoriaComponent implements OnInit {

  formulario: FormGroup = new FormGroup({});
  constructor( public fb: FormBuilder,private dataApi: DataApiService, private router: Router) {
    this.formulario = this.fb.group({
      categoria: ['']
    });
  }

  ngOnInit() {
  }

  categoria: CategoriaModel = new CategoriaModel();

  onSubmit() {
    console.log(this.formulario.value.categoria);
    try {
    this.categoria.nombre= this.formulario.value.categoria;
    console.log(this.categoria.nombre);
    let re = false;
    this.dataApi.registrarCategoria(this.categoria).subscribe(resp => {
      Swal.fire({
        icon: 'success',
        position: 'top-end',
        title: 'Categoria registrada',
        showConfirmButton: false,
        timer: 1000
      });
    }, (err) => {
      console.log(err);
      Swal.fire({
          icon: 'error',
          title: 'Algo ha salido mal',
          text: err.error.error.message,
          footer: 'Vuelve a intentarlo'
         });
    });
  } catch (error) {
    console.log(error);
    Swal.fire({
      icon: 'error',
      title: 'Algo ha salido mal',
      text: 'Hubo un problema con el formulario',
      footer: 'Vuelve a intentarlo'
     });
  }
  }

}
