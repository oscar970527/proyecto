import { Component, OnInit, Inject} from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import Swal from 'sweetalert2';
import { DataApiService } from 'src/app/services/data-api.service';
import { Router } from '@angular/router';
import { CategoriaModel } from 'src/app/models/categoria.model';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-editar-categoria',
  templateUrl: './editar-categoria.component.html',
  styleUrls: ['./editar-categoria.component.css']
})
export class EditarCategoriaComponent implements OnInit {
  formulario: FormGroup = new FormGroup({});
  constructor(public fb: FormBuilder,private dataApi: DataApiService, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.formulario = this.fb.group({
      categoria: ['']
    });
  }

  ngOnInit() {
  }

  categoria: CategoriaModel = new CategoriaModel();

  onSubmit() {
    try {
    this.categoria.nombre= this.formulario.value.categoria;
    console.log(this.categoria.nombre);
    this.dataApi.updateCategoria(this.data.id,this.categoria).subscribe(resp => {
      Swal.fire({
        position: 'top-end',
        title: 'Categoria editada',
        showConfirmButton: false,
        timer: 1000
      });
    }, (err)=>{
      Swal.fire({
        icon: 'error',
        title: 'Algo ha salido mal',
        text: err.error.error.message,
        footer: 'Vuelve a intentarlo'
       });
    });
  } catch (error) {
    Swal.fire({
      icon: 'error',
      title: 'Ups',
      text: 'Se generó un problema en el formulario',
      footer: 'Intenta de nuevo'
     });
     console.log(error);
  }
  }
}
